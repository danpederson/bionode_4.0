#ifndef ADC_H__
#define ADC_H__

/*ADC configuration register definitions*/
/*Using 2/3 prescale*/
#define ADC_8BIT_AIN0 0x0104
#define ADC_8BIT_AIN1 0x0204
#define ADC_8BIT_AIN2 0x0404
#define ADC_8BIT_AIN3 0x0804
#define ADC_8BIT_AIN4 0x1004
#define ADC_8BIT_AIN5 0x2004
#define ADC_8BIT_AIN6 0x4004
#define ADC_8BIT_AIN7 0x8004

#define ADC_10BIT_AIN0 0x0106
#define ADC_10BIT_AIN1 0x0206
#define ADC_10BIT_AIN2 0x0406
#define ADC_10BIT_AIN3 0x0806
#define ADC_10BIT_AIN4 0x1006
#define ADC_10BIT_AIN5 0x2006
#define ADC_10BIT_AIN6 0x4006
#define ADC_10BIT_AIN7 0x8006

#endif