#ifndef BIONODE_H
#define BIONODE_H

#define DEFAULT_RADIO_FREQ 0

#define BIONODE_4V0

#ifdef BIONODE_4V0
  #define RADIO_PACKET_LENGTH 50
  #define RADIO_DATA_OFFSET 5

  #define CH_TOP 27 //AIN1
  #define CH_BOTTOM 3//AIN4
  #define CH_STIM_SENSE_P 4 //AIN5 - This is the optional input
  #define CH_STIM_SENSE_N 5 //AIN6
  #define OPTION_2 26 //AIN0

  #define STIM_GLITCH_SWITCH 21

  #define PSW1 11
  #define PSW2 13

#endif
#endif /* BIONODE_H */