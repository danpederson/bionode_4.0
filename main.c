#include <nRF.h>
#include "spi_master_config.h"
#include "spi_master.h"
#include "twi_master.h"
#include "twi_master_config.h"
#include "bionode.h"
#include "ADC.h"

#define POS_PID_H                      0
#define POS_PID_L                      1
#define POS_TX_ID                      2
#define POS_RX_ID                      3
#define POS_TYPE                       4
#define POS_TARGET_ID                  5
#define POS_COMM_FREQ                  6 //REMOVE THIS REGISTER
#define POS_HANDSHAKE_INTERVAL_H       7 //CONSIDER REMOVING THIS REGISTER
#define POS_HANDSHAKE_INTERVAL_L       8 //CONSIDER REMOVING THIS REGISTER
#define POS_STIM_AMP_H                 9
#define POS_STIM_AMP_L                 10
#define POS_PULSE_REPEAT_TIME_3        11
#define POS_PULSE_REPEAT_TIME_2        12
#define POS_PULSE_REPEAT_TIME_1        13
#define POS_PULSE_REPEAT_TIME_0        14
#define POS_PULSE_WIDTH_H              15
#define POS_PULSE_WIDTH_L              16
#define POS_STIM_CYCLE_H               17
#define POS_STIM_CYCLE_L               18
#define POS_ADC_TIMER1_PRESCALE_REG    19
#define POS_ADC_TIMER1_CC0_REG         20
#define POS_ACTIVE_CHANNELS            21
#define POS_ADC_RESOLUTION             22
#define POS_STIM_TIMER_CONFIG          23 // 0 = Ignore, 1 = Start, 2 = Stop
#define POS_IMPEDANCE_TEST_1_CONFIG    24 // 0 = Ignore, 1 = Start, 2 = Stop
#define POS_IMPEDANCE_TEST_2_CONFIG    25 // 0 = Ignore, 1 = Start, 2 = Stop
#define POS_THERMAL_TEST_CONFIG        26 // 0 = Ignore, 1 = Start, 2 = Stop
#define POS_ADC_CHANNEL_SWITCH_CONFIG  27 // 0 = 1:1, 1 = 1:3, 2 = 1:39
#define POS_POSITIVE_CALIBRATION       28
#define POS_NEGATIVE_CALIBRATION       29
#define POS_POSITIVE_PW_CALIBRATION    30
#define POS_NEGATIVE_PW_CALIBRATION    31

//Added by Chris
#define POS_PRESSURE_READING_CONFIG    32 // 0 = No Pressure Reading, 1 = Pressure Reading
#define POS_POT_CH1_V1_PRESSURE_DATA   33 // data byte for resistance value to digital potentiometer
#define POS_POT_CH1_V2_PRESSURE_DATA   34 // data byte for resistance value to digital potentiometer
#define POS_POT_CH2_V1_PRESSURE_DATA   35 // data byte for resistance value to digital potentiometer
#define POS_POT_CH2_V2_PRESSURE_DATA   36 // data byte for resistance value to digital potentiometer

//Define any additional parameters here...
#define DEFAULT_HANDSHAKE_INTERVAL 100 //Every 0.8 seconds for Fs=5Khz, 8bit resolution
#define SINE_PERIOD_COUNT_LIMIT 5000
uint8_t PRESSURE_ADDRESS = 0xA0;

//Added globals by DanP
uint16_t positiveAmplitudeCode = 0;
uint16_t negativeAmplitudeCode = 0;
uint32_t PRT = 3000;
uint32_t PW = 1000;
uint32_t duration = 50;
uint32_t stimCount = 0;
uint8_t ADC_Active_Ch = 0x0A;
uint32_t ADC_Active_Ch_Config_Reg[2] = {ADC_8BIT_AIN1,ADC_8BIT_AIN4};

uint8_t tx_data[TX_RX_MSG_LENGTH]; //!< SPI TX buffer
uint8_t rx_data[TX_RX_MSG_LENGTH]; //!< SPI RX buffer
uint32_t *spi_base_address;

int ADC_Init(void);
int Radio_Init(uint8_t Freq);
int TIMER1_Init(void);
int Timer2_Init(void);
int PPI_Init(void);
int GPIO_Init(void);
void ADC_IRQHandler(void);
void Stop_Stim(void);

_Bool impedanceTestRunning = false;
_Bool thermalDataReady = false;
uint16_t thermalData = 0x0000;

uint8_t CMD_Data[3];
bool Status;

uint8_t ADC_Results_A[RADIO_PACKET_LENGTH];
uint8_t ADC_Results_B[RADIO_PACKET_LENGTH];
volatile uint8_t RadioRxData[RADIO_PACKET_LENGTH];
const uint8_t Mask1[4]={0xff, 0x3f, 0x0f, 0x3};
const uint8_t Mask2[4]={0xc0, 0xf0, 0xfc, 0xff};	 
uint16_t Handshake_Interval = DEFAULT_HANDSHAKE_INTERVAL;
static uint8_t isRxEvent = 0;
static uint8_t ADC_Resolution = 8;
static _Bool isHandshakeOK=0;
uint16_t PID_Global=0;
static _Bool Reset_ADC_Counter_Flag = 0; //Indicates whether ADC counter shall be reset. This will occur when user switches recording channels
uint8_t ADCChannelSwitchRegister = 0; // 0 = 1:1, 1 = 1:3, 2 = 1:39

void main (void){

 //Product Anomaly. The following two lines of code must be executed to use peripherals
 //Please refer to nRF51822-PAN v2.0 Page 25 entry #26: System: Manual setup is required to enable use of peripherals.
  *(uint32_t *)0x40000504 = 0xC007FFDF;
  *(uint32_t *)0x40006C18 = 0x00008000;

  //Start Crystal Oscillator 
  NRF_CLOCK->TASKS_HFCLKSTART = 0X01;
  while(NRF_CLOCK->EVENTS_HFCLKSTARTED == 0){
  }
  //Ensures that the processor return to sleep after executing interrupt handlers
  SCB->SCR = 0x02;

  ADC_Init();
  Radio_Init(DEFAULT_RADIO_FREQ);
  TIMER1_Init();
  TIMER0_Init();
  Timer2_Init();
  GPIO_Init();
  PPI_Init();
  twi_master_init();
  spi_base_address = spi_master_init(0, SPI_MODE0, false);

  //Init I2C Temp sens.
  CMD_Data[0]=0x01;
  Status = twi_master_transfer(0x90, &CMD_Data[0], 1, true);
  CMD_Data[0]=0;
  CMD_Data[1]=0;
  Status = twi_master_transfer(0x91, &CMD_Data[0], 2, true);

  //Tell the DAC to output zero volts, which is midrange.
  tx_data[0] = (2048>>8);
  tx_data[1] = 0;
  spi_master_tx_rx(spi_base_address, TX_RX_MSG_LENGTH, (const uint8_t *)tx_data, rx_data);

  //Setup and enable interrupts
  NVIC_SetPriority(RADIO_IRQn,2);
  NVIC_SetPriority(ADC_IRQn,3);
  NVIC_SetPriority(TIMER0_IRQn,1);
  NVIC_EnableIRQ(ADC_IRQn);
  NVIC_EnableIRQ(TIMER0_IRQn);
  NVIC_EnableIRQ(RADIO_IRQn);
  NRF_TIMER1->TASKS_START = 1;

  //Go to sleep
  while(1){
  __WFI();
  }
}

int ADC_Init(void){
  NRF_ADC->ENABLE=0x01; //Enable ADC
 //NRF_ADC->CONFIG = 0x0806; //10bit resolution, 2/3 input prescaling, 1.2V Bandgap ref, USE AIN3
 //NRF_ADC->CONFIG = 0x0406; //10bit resolution, 2/3 input prescaling, 1.2V Bandgap ref, USE AIN2
 //NRF_ADC->CONFIG = 0x0422; //10bit resolution, NO input prescaling, 1.2V Bandgap ref, USE AIN2
// NRF_ADC->CONFIG = 0x10422; //10bit resolution, NO input prescaling, AREF0 Ext. ref, USE AIN2
 //NRF_ADC->CONFIG = 0x1002; //10bit resolution, NO input prescaling, 1.2V Bandgap ref, USE AIN4
 //NRF_ADC->CONFIG = 0x11022; //10bit resolution, NO input prescaling, AREF0 Ext. ref, USE AIN4
  //NRF_ADC->CONFIG = ADC_RESOLUTION_10BIT | ADC_INPPSEL_AIN_NO_PS | ADC_REFSEL_VBG|ADC_PSEL_AIN2
  NRF_ADC->CONFIG =ADC_Active_Ch_Config_Reg[0]; //8bit resolution, NO input prescaling, 1.2V Bandgap ref, USE AIN2
  NRF_ADC->INTENSET = 0x01; //Enable interrupt for ADC END event
  NRF_GPIO->DIRCLR = 1<<2; //Ensure P0.02 is set as input
  return 0;
}

int Radio_Init(uint8_t Freq){

  NRF_RADIO->POWER |= 0x01;// Power on radio peripheral
  NRF_RADIO->SHORTS |= 0X03; // Enable shortcuts between Ready/Start, End/Disable
  NRF_RADIO->INTENSET = 0x08;
  NRF_RADIO->PACKETPTR = &ADC_Results_A; //Payload starting address

  NRF_RADIO->TXPOWER = 0x04; //Set Tx power to +4 dBm
  NRF_RADIO->MODE=0X01; // Set on-air data rate to 2Mbps
  NRF_RADIO->PCNF0 = 0x00; // Set the length of S0, Length, S1 fields to 0
  
 //Set the Maximum length of packet payload to be 50 bytes; Set base address length to 2, big Endian
  NRF_RADIO->PCNF1 = 0x1043232;

 //Set the address to 1,1,4
  NRF_RADIO->BASE0 = 0x80201234; 
  NRF_RADIO->PREFIX0 = 0x80;
  NRF_RADIO->TXADDRESS = 0x00;
  NRF_RADIO->RXADDRESSES = 0x01;
  NRF_RADIO->CRCCNF = 0x02; //Two byte CRC
  NRF_RADIO->CRCINIT = 0xFFFF;
  NRF_RADIO->CRCPOLY = 0x11021; //CRC Polynomial = x^16 + x^12 + x^5 + x
  NRF_RADIO->FREQUENCY = Freq;
  NRF_RADIO->EVENTS_BCMATCH = 0;

  return 0;
}

int TIMER1_Init(void){
  NRF_TIMER1->SHORTS = 0x01; // Enable shortcut between COMPARE0 and CLEAR task
  NRF_TIMER1->MODE = 0x00; //Select timer mode
  NRF_TIMER1->PRESCALER = 0x07; //Timer Clock = 16Mhz/2^7 = 125Khz.Set COMPARE0 to 25 to achieve 5khz sampling frequency for ADC
  NRF_TIMER1->BITMODE = 0x00; //16bit timer
  NRF_TIMER1->CC[0] = 25; //Use 125Khz clock to derive 5khz sampling frequency for ADC
  NRF_TIMER1->INTENSET = 1<<16; //Enable COMPARE0 event interrupt.
  return 0;
}

int Timer2_Init(void){
  NRF_TIMER2->SHORTS = 1<<8; // Enable shortcut between COMPARE0 and Stop task
  NRF_TIMER2->MODE = 0x00; //Select timer mode
  NRF_TIMER2->PRESCALER = 0x07; //Timer Clock = 16Mhz/2^7 = 125Khz
  NRF_TIMER2->BITMODE = 0x00; //16bit timer
  NRF_TIMER2->CC[0] = 150; //Firing interval = 1.2ms, This forces Radio to exit Rx mode if it does not hear back from base station
  return 0;
}

int PPI_Init(void){
  //Use timer to trigger ADC sampling
  NRF_PPI->CH[2].EEP = &NRF_TIMER1->EVENTS_COMPARE[0];
  NRF_PPI->CH[2].TEP = &NRF_ADC->TASKS_START;
  NRF_PPI->CHENSET = 1<<2; //Enable PPI Ch0

  //Forces radio to shutdown Rx using Timer2
  NRF_PPI->CH[1].EEP = &NRF_TIMER2->EVENTS_COMPARE[0];
  NRF_PPI->CH[1].TEP = &NRF_RADIO->TASKS_DISABLE; 
  NRF_PPI->CHENSET = 1<<1; //Enable PPI Ch1

  return 0;
}

int GPIO_Init(void){
  NRF_GPIO->DIRSET = (1<<STIM_GLITCH_SWITCH); 
  NRF_GPIO->OUT |= (1<<STIM_GLITCH_SWITCH);
  Stop_Stim();
  return 0;
}

void RADIO_IRQHandler (void){
  static uint32_t temp_ptr;
  NVIC_DisableIRQ(RADIO_IRQn);
  NRF_RADIO->EVENTS_END=0;
  if (isRxEvent==2){
    NVIC_EnableIRQ(RADIO_IRQn);
    return;
  }
  if (!isRxEvent){//If this interrupt is not handling Rx end event (i.e. handling Tx end event) 
    isRxEvent = 1;
    temp_ptr=NRF_RADIO->PACKETPTR;
    NRF_RADIO->PACKETPTR = &RadioRxData;//Switch to Rx pointer
    NRF_RADIO->TASKS_RXEN = 1;
    //NRF_RADIO->SHORTS &= ~(1<<3); //Disable Radio Disabled event->RXEN short
  }
  else{
    if(NRF_RADIO->CRCSTATUS==1){
     // NRF_GPIO->OUTSET=1<<2;
      isHandshakeOK=1;
      if(RadioRxData[1]!=0){ //Make sure PID is not zero
        //TODO: Put parameter parsing and register update code here

        //Byte 4 & 5
        Handshake_Interval =(RadioRxData[POS_HANDSHAKE_INTERVAL_H]<<8) | RadioRxData[POS_HANDSHAKE_INTERVAL_L];

        //Byte 20, 6,7,8,9,10,11,12,13
        if (RadioRxData[POS_STIM_TIMER_CONFIG] == 1)
        {
          NRF_TIMER0->TASKS_STOP = 1;
          NRF_TIMER0->TASKS_CLEAR = 1;
          stimCount = 0;

          positiveAmplitudeCode = (RadioRxData[POS_STIM_AMP_H] <<8) | RadioRxData[POS_STIM_AMP_L];
          negativeAmplitudeCode = 4096 - positiveAmplitudeCode;
          
          int8_t posCalibration = RadioRxData[POS_POSITIVE_CALIBRATION];
          int8_t negCalibration = RadioRxData[POS_NEGATIVE_CALIBRATION];

          positiveAmplitudeCode = positiveAmplitudeCode + posCalibration;
          negativeAmplitudeCode = negativeAmplitudeCode + negCalibration;

          PRT = (RadioRxData[POS_PULSE_REPEAT_TIME_3] <<24) |(RadioRxData[POS_PULSE_REPEAT_TIME_2] <<16) | (RadioRxData[POS_PULSE_REPEAT_TIME_1] <<8) | RadioRxData[POS_PULSE_REPEAT_TIME_0]; 
          PW = (RadioRxData[POS_PULSE_WIDTH_H] <<8) | RadioRxData[POS_PULSE_WIDTH_L];
          duration = (RadioRxData[POS_STIM_CYCLE_H]<<8) | RadioRxData[POS_STIM_CYCLE_L];

          //double the PRT
          PRT = PRT<<1;

          int8_t PW1Cal = RadioRxData[POS_NEGATIVE_PW_CALIBRATION];
          int8_t PW2Cal = RadioRxData[POS_POSITIVE_PW_CALIBRATION];

          NRF_TIMER0->CC[0] = PRT;
          NRF_TIMER0->CC[1] = PRT-(PW+PW1Cal);
          NRF_TIMER0->CC[2] = PRT>>1;
          NRF_TIMER0->CC[3] = (PRT>>1)-(PW+PW2Cal);

          NRF_GPIO->OUT |= (1<<STIM_GLITCH_SWITCH);

          NRF_TIMER0->TASKS_START = 1;
        }
        else if (RadioRxData[POS_STIM_TIMER_CONFIG] == 2)
        {
          NRF_TIMER0->TASKS_STOP = 1;
          NRF_TIMER0->TASKS_CLEAR = 1;
          Stop_Stim();
          stimCount = 0;
        }

        if (RadioRxData[POS_THERMAL_TEST_CONFIG] == 1)
        {
          //Make a thermal measurement.
          CMD_Data[0]=0x00;
          Status = twi_master_transfer(0x92, &CMD_Data[0], 1, true);
          CMD_Data[0]=0; 
          CMD_Data[1]=0;
          Status = twi_master_transfer(0x93, &CMD_Data[0], 2, true);
          thermalData = (CMD_Data[0]<<8 | CMD_Data[1])>>4;
          //Set thermal measurement ready flag
          thermalDataReady = true;
        }

        if (RadioRxData[POS_PRESSURE_READING_CONFIG] != 0)
        {
          //set digi pots
          CMD_Data[0]= 0x00;
          CMD_Data[1]= RadioRxData[POS_POT_CH1_V1_PRESSURE_DATA];
          Status = twi_master_transfer(PRESSURE_ADDRESS, &CMD_Data[0], 2, true);    

          CMD_Data[0]= 0x01;
          CMD_Data[1]= RadioRxData[POS_POT_CH2_V1_PRESSURE_DATA];
          Status = twi_master_transfer(PRESSURE_ADDRESS, &CMD_Data[0], 2, true);          

          //set digi pots
          //CMD_Data[0]= RadioRxData[POS_PRESSURE_COMMAND];//0x71;//0x11;
          //CMD_Data[1]= RadioRxData[POS_PRESSURE_DATA];
          //Status = twi_master_transfer(RadioRxData[POS_PRESSURE_ADDRESS], &CMD_Data[0], 2, true);
        }


        //Byte 14 and 15
        if ((NRF_TIMER1->CC[0] != RadioRxData[POS_ADC_TIMER1_CC0_REG]) ||
        (NRF_TIMER1->PRESCALER != RadioRxData[POS_ADC_TIMER1_PRESCALE_REG])){
          
          NRF_TIMER1->TASKS_STOP=1;
          NRF_TIMER1->TASKS_CLEAR=1;
          NRF_TIMER1->PRESCALER=RadioRxData[POS_ADC_TIMER1_PRESCALE_REG];
          NRF_TIMER1->CC[0]=RadioRxData[POS_ADC_TIMER1_CC0_REG];
          NRF_TIMER1->TASKS_START=1;
        }
        /* Byte 16 and Byte 17
          This byte is in the format of XXBBBAAA, XX are don't cares, AAA selects one of the 8 Ch's and will go to the 
          even indices of the radio packet, BBB selects one of the 8 ch's  and goes to the odd indices
          Also updates ADC Resolution
        */
        if ( (ADC_Active_Ch != RadioRxData[POS_ACTIVE_CHANNELS]) || (ADC_Resolution != RadioRxData[POS_ADC_RESOLUTION]) || (ADCChannelSwitchRegister != RadioRxData[POS_ADC_CHANNEL_SWITCH_CONFIG]))
        {
          ADC_Active_Ch = RadioRxData[POS_ACTIVE_CHANNELS];
          ADC_Resolution = RadioRxData[POS_ADC_RESOLUTION];
          NRF_ADC->TASKS_STOP = 1;
          NVIC_DisableIRQ(ADC_IRQn);
          NVIC_ClearPendingIRQ(ADC_IRQn);
          switch (0x07 & ADC_Active_Ch)//Check the even Ch selection
          {
            case 1: 
              ADC_Active_Ch_Config_Reg[0] = (ADC_Resolution==8) ? ADC_8BIT_AIN4 : ADC_10BIT_AIN4;
              break;
            case 2:
              ADC_Active_Ch_Config_Reg[0] = (ADC_Resolution==8) ? ADC_8BIT_AIN1 : ADC_10BIT_AIN1;
              break;
            case 3:
              ADC_Active_Ch_Config_Reg[0] = (ADC_Resolution==8) ? ADC_8BIT_AIN5 : ADC_10BIT_AIN5;
              break;
            case 4:
              ADC_Active_Ch_Config_Reg[0] = (ADC_Resolution==8) ? ADC_8BIT_AIN6 : ADC_10BIT_AIN6;
              break;
            case 5:
              ADC_Active_Ch_Config_Reg[0] = (ADC_Resolution==8) ? ADC_8BIT_AIN0 : ADC_10BIT_AIN0;
              break;
            
            default:
              break;
          }

          switch (0x07 & (ADC_Active_Ch>>3)) //Check the odd Ch selection
          {
            case 1: 
              ADC_Active_Ch_Config_Reg[1] = (ADC_Resolution==8) ? ADC_8BIT_AIN4 : ADC_10BIT_AIN4;
              break;
            case 2:
              ADC_Active_Ch_Config_Reg[1] = (ADC_Resolution==8) ? ADC_8BIT_AIN1 : ADC_10BIT_AIN1;
              break;
            case 3:
              ADC_Active_Ch_Config_Reg[1] = (ADC_Resolution==8) ? ADC_8BIT_AIN5 : ADC_10BIT_AIN5;
              break;
            case 4:
              ADC_Active_Ch_Config_Reg[1] = (ADC_Resolution==8) ? ADC_8BIT_AIN6 : ADC_10BIT_AIN6;
              break;
            case 5:
              ADC_Active_Ch_Config_Reg[1] = (ADC_Resolution==8) ? ADC_8BIT_AIN0 : ADC_10BIT_AIN0;
              break;

            default:
              break;
          }

          ADCChannelSwitchRegister = RadioRxData[POS_ADC_CHANNEL_SWITCH_CONFIG];

          Reset_ADC_Counter_Flag = 1;
          NRF_ADC->CONFIG = ADC_Active_Ch_Config_Reg[0];
          NVIC_EnableIRQ(ADC_IRQn);
        }
      }// end of if(RadioRxData[1]!=0)
  //   NRF_GPIO->OUTCLR=1<<2; 
     NRF_RADIO->PACKETPTR = temp_ptr;//Set it back to pointers for TX
    }
  }
  NVIC_EnableIRQ(RADIO_IRQn);
}
void ADC_IRQHandler(void){
  uint8_t i;
  static uint8_t ADC_Counter = 0;
  static uint8_t Cnt_I = 0;
  static uint8_t Cnt_O = 0;
  static uint16_t PID = 0;
  static uint16_t Temp_Val=0;
  static _Bool Buffer_A_Select = 1;
  static uint8_t HandshakeRetry_cnt=0;
  NVIC_DisableIRQ(ADC_IRQn);
  NRF_ADC->EVENTS_END = 0;
  Temp_Val = 0x3ff & NRF_ADC->RESULT;
  NRF_ADC->TASKS_STOP= 1; 
  if (Reset_ADC_Counter_Flag == 1){
    ADC_Counter = 0;
    Cnt_I=0;Cnt_O=0; 
    Reset_ADC_Counter_Flag =0;
  }  //Reset ADC counter because user switched recording ch or ADC resolution is changed
  
  //Here is where the ADC Active channel is swapped.
  if (ADCChannelSwitchRegister == 0) //Every other datapoint should be from ADC register 0
  {
    if (NRF_ADC->CONFIG == ADC_Active_Ch_Config_Reg[0])
    {
      NRF_ADC->CONFIG = ADC_Active_Ch_Config_Reg[1];
    }
    else
    {
      NRF_ADC->CONFIG = ADC_Active_Ch_Config_Reg[0];
    }
  }
  else if (ADCChannelSwitchRegister == 1)
  {
    if ((ADC_Counter&0x3) == 0x3) //Every 4th datapoint should be from ADC register 0.
    {
      NRF_ADC->CONFIG = ADC_Active_Ch_Config_Reg[0];
    }
    else
    {
      NRF_ADC->CONFIG = ADC_Active_Ch_Config_Reg[1];
    }
  }
  else
  {
    if (ADC_Counter == 39)
    {
      NRF_ADC->CONFIG = ADC_Active_Ch_Config_Reg[0]; //Every 40th datapoint should be from ADC register 0.
    }
    else
    {
      NRF_ADC->CONFIG = ADC_Active_Ch_Config_Reg[1];
    }
  }

  if (Buffer_A_Select)
  {
    if (ADC_Resolution == 8){
      ADC_Results_A[ADC_Counter+RADIO_DATA_OFFSET]= 0xff &Temp_Val;
    }
    else if (ADC_Resolution == 10){
      if ((ADC_Counter == 0)||(ADC_Counter == 4)||(ADC_Counter == 8)||(ADC_Counter == 12)||(ADC_Counter == 16)||(ADC_Counter == 20)||(ADC_Counter == 24)||(ADC_Counter == 28)){
        ADC_Results_A[Cnt_I+Cnt_O*5+RADIO_DATA_OFFSET] = 0;
        ADC_Results_A[Cnt_I+Cnt_O*5+RADIO_DATA_OFFSET +1] = 0;
        ADC_Results_A[Cnt_I+Cnt_O*5+RADIO_DATA_OFFSET +2] = 0;
        ADC_Results_A[Cnt_I+Cnt_O*5+RADIO_DATA_OFFSET +3] = 0;
        ADC_Results_A[Cnt_I+Cnt_O*5+RADIO_DATA_OFFSET +4] = 0;
      }
      ADC_Results_A[Cnt_I+Cnt_O*5+RADIO_DATA_OFFSET] |= (uint8_t)((Temp_Val>>(2*(Cnt_I+1)))&Mask1[Cnt_I]);
      ADC_Results_A[Cnt_I+Cnt_O*5+RADIO_DATA_OFFSET+1] |=(uint8_t) ((Temp_Val<<(6-2*Cnt_I))&Mask2[Cnt_I]);
      Cnt_I++;
      if (Cnt_I == 4){
        Cnt_I=0;
        Cnt_O++;
      }
    }/*End if else if (ADC_Resolution == 10)*/
  }/*End of if(Buffer_A_Select)*/
  else {
    if (ADC_Resolution == 8){
      ADC_Results_B[ADC_Counter + RADIO_DATA_OFFSET]= 0xff & Temp_Val;
    }
    else if (ADC_Resolution == 10){
      if ((ADC_Counter == 0)||(ADC_Counter == 4)||(ADC_Counter == 8)||(ADC_Counter == 12)||(ADC_Counter == 16)||(ADC_Counter == 20)||(ADC_Counter == 24)||(ADC_Counter == 28)){
        ADC_Results_B[Cnt_I+Cnt_O*5+RADIO_DATA_OFFSET] = 0;
        ADC_Results_B[Cnt_I+Cnt_O*5+RADIO_DATA_OFFSET + 1] = 0;
        ADC_Results_B[Cnt_I+Cnt_O*5+RADIO_DATA_OFFSET + 2] = 0;
        ADC_Results_B[Cnt_I+Cnt_O*5+RADIO_DATA_OFFSET + 3] = 0;
        ADC_Results_B[Cnt_I+Cnt_O*5+RADIO_DATA_OFFSET + 4] = 0;
      }
      ADC_Results_B[Cnt_I+Cnt_O*5+RADIO_DATA_OFFSET] |= (uint8_t)((Temp_Val>>(2*(Cnt_I+1)))&Mask1[Cnt_I]);
      ADC_Results_B[Cnt_I+Cnt_O*5+RADIO_DATA_OFFSET +1] |=(uint8_t) ((Temp_Val<<(6-2*Cnt_I))&Mask2[Cnt_I]);
      Cnt_I++;
      if (Cnt_I == 4){
          Cnt_I=0;
          Cnt_O++;
      }  
    }/*End of else if (ADC_Resolution == 10)*/
  }/*End of else*/
  ADC_Counter++;
  if (  ((ADC_Counter == 40)&&(ADC_Resolution ==8)) ||  ((ADC_Counter == 32)&&(ADC_Resolution ==10))  ){
    Cnt_I=0;
    Cnt_O=0;
    if(((PID%Handshake_Interval) ==1)&&(isHandshakeOK==0)){
      PID--;
      HandshakeRetry_cnt++;
      if (HandshakeRetry_cnt>10){
        HandshakeRetry_cnt=0;
        PID=0;
      }
    } /*End of if(((PID%Handshake_Interval) ==1)&&(isHandshakeOK==0))*/
    else if(isHandshakeOK==1){
      HandshakeRetry_cnt=0;
    }
    if ((PID%Handshake_Interval) ==0){
      //NRF_RADIO->SHORTS |= 1<<3;//Enable Start radio Rx immediately after Tx finishes
      isRxEvent=0;
      isHandshakeOK=0;
    }
    else{isRxEvent=2;}
    if (Buffer_A_Select){
      ADC_Results_A[0] = (uint8_t) 0xff&(PID>>8);
      ADC_Results_A[1] = (uint8_t) 0xff&PID;
      ADC_Results_A[2] = (uint8_t) 0x02;
      ADC_Results_A[3] = (uint8_t) 0x00;
      //Set type.
      //8-bit = 0
      //10-bit = 1
      //Impedance = 2
      //Thermal = 3
      if (impedanceTestRunning)
      {
        ADC_Results_A[4] = 0x02;
      }
      else if (thermalDataReady)
      {
        ADC_Results_A[4] = 0x03;
        ADC_Results_A[RADIO_DATA_OFFSET + 40] = thermalData>>8;
        ADC_Results_A[RADIO_DATA_OFFSET + 41] = thermalData&0xFF;
        thermalDataReady = false;
      }
      else
      {
        ADC_Results_A[4] = ADC_Resolution == 8 ? (uint8_t)0x00 : (uint8_t)0x01;
      }
      NRF_RADIO->PACKETPTR = &ADC_Results_A; 
    }
    else{
      ADC_Results_B[0] = (uint8_t) 0xff&(PID>>8);
      ADC_Results_B[1] = (uint8_t) 0xff&PID;
      ADC_Results_B[2] = (uint8_t) 0x02;
      ADC_Results_B[3] = (uint8_t) 0x00;
      //Set type.
      //8-bit = 0
      //10-bit = 1
      //Impedance = 2
      //Thermal = 3
      if (impedanceTestRunning)
      {
        ADC_Results_B[4] = 0x02;
      }
      else if (thermalDataReady)
      {
        ADC_Results_B[4] = 0x03;
        ADC_Results_B[RADIO_DATA_OFFSET + 40] = thermalData>>8;
        ADC_Results_B[RADIO_DATA_OFFSET + 41] = thermalData&0xFF;
        thermalDataReady = false;
      }
      else
      {
        ADC_Results_B[4] = ADC_Resolution == 8 ? (uint8_t)0x00 : (uint8_t)0x01;
      }
      NRF_RADIO->PACKETPTR = &ADC_Results_B; 
    }
    NRF_TIMER2->TASKS_STOP=1;
    NRF_TIMER2->TASKS_CLEAR=1;
    NRF_TIMER2->TASKS_START=1;
    NRF_RADIO->TASKS_TXEN = 0x01;
    ADC_Counter = 0;
    if (PID==0xffff){PID=1;}
    else{PID++;}
    Buffer_A_Select=!Buffer_A_Select;

  }

  PID_Global=PID;
 // NRF_GPIO->OUTCLR = 1<<18;  //TODO: Remove me: For test purpose only 
  NVIC_EnableIRQ(ADC_IRQn);
}

void TIMER0_IRQHandler(void)
{
  NVIC_DisableIRQ(TIMER0_IRQn);

  if (NRF_TIMER0->EVENTS_COMPARE[1] == 1)
  {
    //Positive Stim
    tx_data[0] = positiveAmplitudeCode>>8;
    tx_data[1] = positiveAmplitudeCode;
    spi_master_tx_rx(spi_base_address, TX_RX_MSG_LENGTH, (const uint8_t *)tx_data, rx_data);
  }

  else if (NRF_TIMER0->EVENTS_COMPARE[2] == 1)
  {
    tx_data[0] = (2048>>8);
    tx_data[1] = 0;
    spi_master_tx_rx(spi_base_address, TX_RX_MSG_LENGTH, (const uint8_t *)tx_data, rx_data);
    stimCount = stimCount + 1;
  }

  else if (NRF_TIMER0->EVENTS_COMPARE[3] == 1)
  {
    //Negative Stim
    tx_data[0] = negativeAmplitudeCode>>8;
    tx_data[1] = negativeAmplitudeCode;
    spi_master_tx_rx(spi_base_address, TX_RX_MSG_LENGTH, (const uint8_t *)tx_data, rx_data);
  }

  else if (NRF_TIMER0->EVENTS_COMPARE[0] == 1)
  {
    tx_data[0] = (2048>>8); //TRUNC(4096*(DAC_OUT+1.8)/3) = 0 ---> DAC_OUT = 2457.
    tx_data[1] = 0;
    spi_master_tx_rx(spi_base_address, TX_RX_MSG_LENGTH, (const uint8_t *)tx_data, rx_data);
    stimCount = stimCount + 1;
  }

  NRF_TIMER0->EVENTS_COMPARE[3] = 0;
  NRF_TIMER0->EVENTS_COMPARE[2] = 0;
  NRF_TIMER0->EVENTS_COMPARE[1] = 0;
  NRF_TIMER0->EVENTS_COMPARE[0] = 0;
  //If the stimCount is greater than or equal to the set stim duration, stop and clear the timer and reset the stimCount
  if ((stimCount >= duration) && (duration != 0xFFFF))
  {
    Stop_Stim();
    NRF_TIMER0->TASKS_STOP = 0x01;
    NRF_TIMER0->TASKS_CLEAR = 0x01;
    stimCount = 0;
  }

  NVIC_EnableIRQ(TIMER0_IRQn);
}

int TIMER0_Init(void)
{
  NRF_TIMER0->SHORTS = 0x01; //Enable shortcut between COMPARE1 and CLEAR task
  NRF_TIMER0->MODE = 0x00; //Select timer mode
  NRF_TIMER0->PRESCALER = 0x4; //Timer Clock = 16Mhz/2^4 = 1 MHz.
  NRF_TIMER0->BITMODE = 0x3; //32bit timer
  NRF_TIMER0->CC[0] = PRT;
  NRF_TIMER0->CC[1] = PRT-PW;
  NRF_TIMER0->CC[2] = PRT>>1;
  NRF_TIMER0->CC[3] = (PRT>>1)-PW;
  NRF_TIMER0->INTENSET = 0xF<<16; //Enable COMPARE0, COMPARE1, COMPARE2, and COMPARE3 event interrupts
}

__inline void Stop_Stim(void){
  //Set DAC Out to zero
  tx_data[0] = (2048>>8);
  tx_data[1] = 0;
  spi_master_tx_rx(spi_base_address, TX_RX_MSG_LENGTH, (const uint8_t *)tx_data, rx_data);
  //Close the stimSwitch.
  NRF_GPIO->OUT &= ~(1<<STIM_GLITCH_SWITCH);
  return;
}