/*****************************************************************************
 * Copyright (c) 2013 Rowley Associates Limited.                             *
 *                                                                           *
 * This file may be distributed under the terms of the License Agreement     *
 * provided with this software.                                              *
 *                                                                           *
 * THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND, INCLUDING THE   *
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. *
 *****************************************************************************/
#define STARTUP_FROM_RESET
.macro ISR_HANDLER name=
  .section .vectors, "ax"
  .word \name
  .section .init, "ax"
  .thumb_func
  .weak \name
\name:
1: b 1b /* endless loop */
.endm

.macro ISR_RESERVED
  .section .vectors, "ax"
  .word 0
.endm

  .syntax unified
  .global reset_handler

  .section .vectors, "ax"
  .code 16
  .global _vectors

_vectors:
  .word __stack_end__
#ifdef STARTUP_FROM_RESET
  .word reset_handler
#else
  .word reset_wait
#endif /* STARTUP_FROM_RESET */
ISR_HANDLER NMI_Handler
ISR_HANDLER HardFault_Handler
ISR_HANDLER MemManage_Handler 
ISR_HANDLER BusFault_Handler
ISR_HANDLER UsageFault_Handler
ISR_RESERVED
ISR_RESERVED
ISR_RESERVED
ISR_RESERVED
ISR_HANDLER SVC_Handler
ISR_HANDLER DebugMon_Handler
ISR_RESERVED
ISR_HANDLER PendSV_Handler
ISR_HANDLER SysTick_Handler
  // External interrupts start her 
ISR_HANDLER POWER_CLOCK_IRQHandler
ISR_HANDLER RADIO_IRQHandler
ISR_HANDLER UART0_IRQHandler
ISR_HANDLER SPI0_TWI0_IRQHandler
ISR_HANDLER SPI1_TWI1_IRQHandler
ISR_RESERVED
ISR_HANDLER GPIOTE_IRQHandler
ISR_HANDLER ADC_IRQHandler
ISR_HANDLER TIMER0_IRQHandler
ISR_HANDLER TIMER1_IRQHandler
ISR_HANDLER TIMER2_IRQHandler
ISR_HANDLER RTC0_IRQHandler
ISR_HANDLER TEMP_IRQHandler
ISR_HANDLER RNG_IRQHandler
ISR_HANDLER ECB_IRQHandler
ISR_HANDLER CCM_AAR_IRQHandler
ISR_HANDLER WDT_IRQHandler
ISR_HANDLER RTC1_IRQHandler
ISR_HANDLER QDEC_IRQHandler
ISR_HANDLER WUCOMP_COMP_IRQHandler
ISR_HANDLER SWI0_IRQHandler
ISR_HANDLER SWI1_IRQHandler
ISR_HANDLER SWI2_IRQHandler
ISR_HANDLER SWI3_IRQHandler
ISR_HANDLER SWI4_IRQHandler
ISR_HANDLER SWI5_IRQHandler
ISR_RESERVED
ISR_RESERVED
ISR_RESERVED
ISR_RESERVED
ISR_RESERVED
ISR_RESERVED

  .section .init, "ax"
  .thumb_func

  reset_handler:

#ifndef __NO_SYSTEM_INIT
  ldr r0, =__RAM_segment_start__
  ldr r1, =0x10000000 // FICR
  ldr r1, [r1, #0x38] // SIZERAMBLOCK[0]
  add r0, r1
  mov sp, r0
  bl SystemInit
#endif

  b _start

#ifndef __NO_SYSTEM_INIT
  .thumb_func
  .weak SystemInit
SystemInit:
  bx lr
#endif

#ifndef STARTUP_FROM_RESET
  .thumb_func
reset_wait:
1: b 1b /* endless loop */
#endif /* STARTUP_FROM_RESET */
